# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Configuration pour le nodemanager
  config.vm.define "nodemanager" do |nodemanager|
    nodemanager.vm.box = "generic/debian11"
    nodemanager.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", "2048"]
      v.gui = true
    end
    nodemanager.vm.hostname = "nodemanager"
    nodemanager.vm.network "private_network", ip: "192.168.5.10"
    nodemanager.vm.provision "shell", inline: <<-SHELL
    sudo export HOME=/root
    sudo export DEBIAN_FRONTEND=noninteractive
    #Installation des dependances
    sudo apt-get update -y  
    sudo apt-get install -y python3 python3-pip
    echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu focal main" | sudo tee -a /etc/apt/sources.list.d/ansible.list
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
    sudo apt-get update -y && sudo apt-ge upgrade -y
    sudo apt-get install ansible tree git tmux -y
    #renseignement des fichiers /etc/ansible/hosts et /etc/hosts pour la résolution DNS
    sudo echo -e "192.168.5.11 firstnode\n192.168.5.12 secondnode" | sudo tee -a /etc/hosts
    sudo echo -e "[nodeslaves]\nfirstnode ansible_python_interpreter=/usr/bin/python3\nsecondnode ansible_python_interpreter=/usr/bin/python3" | sudo tee -a /etc/ansible/hosts
    #Parametrage du fichier de configuration sshd_config
    sudo sed -i 's/PermitRootLogin yes/PermitRootLogin no' /etc/ssh/sshd_config
    sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config
    sudo systemctl reload sshd
    sudo alias ssha='eval $(ssh-agent) && ssh-add' >> ~/.bashrc
    SHELL
  end
  
  # Configuration pour le premier noeud
  config.vm.define "firstnode" do |firstnode|
    firstnode.vm.box = "generic/debian11"
    firstnode.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", "2048"]
      v.gui = true
    end
    firstnode.vm.hostname = "firstnode"
    firstnode.vm.network "private_network", ip: "192.168.5.11"
    firstnode.vm.provision "shell", inline: <<-SHELL
    sudo export HOME=/root
    sudo export DEBIAN_FRONTEND=noninteractive
    # Installation de python nécessaire à Ansible
    sudo apt-get update -y
    sudo apt-get install -y python3 python3-pip
    sudo pip3 install docker
    # Installation de docker:
    for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
    sudo apt-get install ca-certificates curl gnupg -y
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    # Ajout du dépôt docker aux sources :
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update -y
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
    sudo usermod -aG docker $USER
    sudo docker run hello-worlds
    #Parametrage du fichier de configuration sshd_config
    sudo sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
    sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config
    sudo systemctl reload sshd
    SHELL
  end

  # Configuration pour le second noeud
  config.vm.define "secondnode" do |secondnode|
    secondnode.vm.box = "generic/debian11"
    secondnode.vm.provider "virtualbox" do |v|
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", "2048"]
      v.gui = true
    end
    secondnode.vm.hostname = "secondnode"
    secondnode.vm.network "private_network", ip: "192.168.5.12"
    secondnode.vm.provision "shell", inline: <<-SHELL
      export HOME=/root
      export DEBIAN_FRONTEND=noninteractive
      # Installation de python nécessaire à Ansible
      sudo apt-get update -y
      sudo apt-get install -y python3 python3-pip
      sudo pip3 install docker
      #Installation de docker
      for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
      sudo apt-get install ca-certificates curl gnupg -y
      sudo install -m 0755 -d /etc/apt/keyrings
      sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
      sudo chmod a+r /etc/apt/keyrings/docker.gpg
      # Ajout du dépôt docker aux sources :
      echo \
        "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
      sudo apt-get update -y
      sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
      sudo usermod -aG docker $USER
      sudo docker run hello-world
      #Parametrage du fichier de configuration sshd_config
      sudo sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
      sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config
      sudo systemctl reload sshd
    SHELL
  end
end
